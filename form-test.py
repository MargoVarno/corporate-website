from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException

service = Service()
# Инициализация WebDriver с указанием пути к ChromeDriver
chrome_driver_path = r".\chromedriver"
# Создайте объект ChromeOptions
chrome_options = Options()
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-dev-shm-usage')
chrome_options.add_argument("--headless")

# Инициализируйте драйвер, передав объект ChromeOptions
dr = webdriver.Chrome(options=chrome_options)

# Откройте веб-страницу
dr.get("https://smartapp.technology/")

try:
    # Найдите элемент формы (замените на свой селектор)
    form_element = dr.find_element(By.CSS_SELECTOR, ".form-block.contact_form_block")

    # Используйте JavaScript для выполнения прокрутки к элементу формы
    dr.execute_script("arguments[0].scrollIntoView({behavior: 'smooth', block: 'end', inline: 'nearest'});", form_element)

    # Найдите поле email и выведите информацию о найденном элементе
    email_field = WebDriverWait(dr, 10).until(
        EC.visibility_of_element_located((By.NAME, "email-field"))
    )
    print(email_field)

    # Очистите поле email
    email_field.clear()

    # Заполните поле email значением "sat@mailto.plus"
    email_field.send_keys("sat@mailto.plus")

except TimeoutException:
    print("Элемент не был найден в течение 10 секунд.")

# Добавлена задержка перед закрытием браузера
time.sleep(10)  # Замените 10 на нужное вам количество секунд

# Закройте браузер
dr.quit()
